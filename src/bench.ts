import { ResNames } from "./gl2d/resnames"
import { Gl2d } from "./gl2d/index"
import { ResMan } from "./gl2d/resources/resman"
import { SpriteRenderer } from "./gl2d/render/spriterenderer"
import { View } from "./gl2d/render/view"
import { ITexture } from "./gl2d/interfaces"
import { ISpriteRenderer } from "./gl2d/interfaces"
import * as Benchmark from "benchmark"

Gl2d.Init();
let canvas = document.getElementById("gl2d") as HTMLCanvasElement
let view = new View(canvas)
let rend1 = new SpriteRenderer(view,1000)
let rend2 = new SpriteRenderer(view,1)
view.clear()
let weasel = ResMan.instance.loadResource(ResNames.TEXTURE, "/img/weasel.png") as ITexture
let turnip = ResMan.instance.loadResource(ResNames.TEXTURE, "/img/turnip.jpeg") as ITexture

function drawTest(rend:ISpriteRenderer, stripe:boolean, checker:boolean,rand:boolean){
  view.clear()
  let toggle= false

  for(let y =0; y <76; y++){
    for(let x =0; x <102; x++){
      rend.draw(toggle ? weasel: turnip, x * 10 ,y * 10,10,10) 
          if (checker) toggle = !toggle

      if (rand && Math.random() < 0.01) toggle=!toggle

    
    }
    if (stripe) toggle = !toggle
  }
}


ResMan.instance.registerNotify(()=>{ 
 
let noBuf = new SpriteRenderer(view,1)
let smallBuf = new SpriteRenderer(view,50)
let medBuff = new SpriteRenderer(view,100)
let largeBuff = new SpriteRenderer(view,200)
let hugeBuff = new SpriteRenderer(view,500)



let checker = false
let stripe = false
let rand = true

let suite = new Benchmark.Suite
suite

.add('huge cache', function():void {
  drawTest( hugeBuff, stripe, checker,rand)
})
.add('large cache', function():void {
  drawTest( largeBuff, stripe, checker,rand)
})
.add('medium cache', function():void {
  drawTest( medBuff, stripe, checker,rand)
})

.add('small cache', function():void{
  drawTest( smallBuf, stripe,  checker,rand)
})
.add('no cache', function():void {
  drawTest( noBuf, stripe, checker, rand)
})

// add listeners
.on('cycle', function(event):void {
  console.log(String(event.target))
})
.on('complete', function():void {
  console.log('Fastest is ' + this.filter('fastest').map('name'))
})
// run async
.run({ 'async': true })

})
