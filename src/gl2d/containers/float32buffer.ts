export class Float32Buffer{
  static readonly DefaultBufferSize = 2000;
  public readonly values:Float32Array
  public length:number = 0

  public constructor(public readonly size:number = Float32Buffer.DefaultBufferSize){
    this.values = new Float32Array(size)
  }

  public LoadVerts(verts:Array<number>):void{
    //TODO: overflow check...
    for(let coord of verts){
      this.values[this.length++] = coord
    }
  }
  public Clear():void{
    this.length = 0;
  }


}