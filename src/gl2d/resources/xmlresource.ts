import { IResource, IResMan } from "../interfaces"

export abstract class XmlResource implements IResource{

  protected loaded:boolean


  abstract parseXml(doc:XMLDocument):void

  protected constructor(public readonly url:string, public readonly resMan:IResMan){
    var xhttp = new XMLHttpRequest()
    xhttp.overrideMimeType("text/xml")
    xhttp.onreadystatechange = () => {
      if (xhttp.readyState == 4 && xhttp.status == 200) {
        this.parseXml(xhttp.responseXML)
        this.loaded = true
        resMan.notifyComplete(this)
      }
    }
    xhttp.open("GET", url, true)
    xhttp.send()
  }
}