import { IResMan, IResource, ITexture } from "../interfaces";

type ResConstructor = (url:string,rm:IResMan)=> IResource
type ResourceNotify = ()=> void

export class ResMan implements IResMan {

  isComplete(): boolean {
    return this.loadCount == 0
  }

  private loadCount = 0
  public static instance = new ResMan()
  private readonly resources = new Map<string, IResource>()
  private readonly resTypes = new Map<string,ResConstructor>()
  private readonly notifyList = new Array<ResourceNotify>()

  private constructor(){}

  public notifyComplete(res: IResource):void {
    this.loadCount--
    while(this.loadCount == 0 && this.notifyList.length > 0)
      this.notifyList.pop()()
  }

  public loadResource(typeName:string, url:string):IResource{
    this.loadCount++
    let res = this.resources.get(url)
    if (res == null){
      res = this.resTypes[typeName](url, this)
      this.resources.set(url, res)
    }
    else{
      this.notifyComplete(res)
    }
    return res
  }

  public registerResourceType(name:string, con:ResConstructor){
    this.resTypes[name] = con
  }

  public registerNotify(cb:ResourceNotify){
    this.notifyList.push(cb)
  }

}