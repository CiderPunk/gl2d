export namespace XmlParser{

  export function parse<T>(xml:XMLDocument):T{
    return readXml(xml.rootElement) as T
  }

  function readXml(xml:Node):any{
    // Create the return object
    let obj = {};

    if (xml.nodeType == 1) { // element
      // do attributes
      if (xml.attributes.length > 0) {
      obj["@attributes"] = {}
        for (var j = 0; j < xml.attributes.length; j++) {
          var attribute = xml.attributes.item(j)
          obj["@attributes"][attribute.nodeName] = attribute.nodeValue
        }
      }
    } else if (xml.nodeType == 3) { // text
      obj = xml.nodeValue
    }
    // do children
    if (xml.hasChildNodes()) {
      for(let item of xml.childNodes){
        let nodeName = item.nodeName
        if (obj[nodeName] == undefined) {
          obj[nodeName] = readXml(item)
        } else {
          if (obj[nodeName].push == undefined) {
            var old = obj[nodeName];
            obj[nodeName] = [];
            obj[nodeName].push(old);
          }
          obj[nodeName].push(readXml(item));
        }
      }
    }
    return obj
  }
}
