export namespace PathUtils{

  export function getReferedPath(referer:string, target:string):string{
    return referer.substr(0,referer.lastIndexOf("/")+1) + target
  }

}