//import { Texture } from "./texture";
import { ITexture } from "../interfaces";

export class TxPart implements ITexture{

  get url():string{
    return this.t.url
  }

  private readonly tx:Array<number>

  getTexture(gl: WebGL2RenderingContext): WebGLTexture {
   return this.t.getTexture(gl)
  }
  getTexCoords() {
    return this.tx
  }
 
  public constructor(private t:ITexture, x:number, y:number, w:number, h:number){
    this.tx = [
      x,y,
      x+w,y,
      x,y+h,
      x,y+h,
      x+w,y,
      x+w,y+h,
    ]
  }

}