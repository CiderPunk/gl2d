import { ITexture, IResMan } from "../interfaces";

export class Texture implements ITexture {

  loaded = false
  tex:WebGLTexture
  image:HTMLImageElement

  public static loader(url:string, resman:IResMan):Texture{
    return new Texture(url, resman)
  }

  private constructor(public readonly url: string, private readonly resman: IResMan) {
    this.image = new Image()
    this.image.src = url
    this.image.onload = ()=>{
      this.loaded = true;
      resman.notifyComplete(this)
    }
  }

public getTexCoords():Array<number>{
  return [
     0,0,
     1,0,
     0,1,
     0,1,
     1,0,
     1,1,
    ]
}

  public getTexture(gl:WebGL2RenderingContext):WebGLTexture{
    if (this.tex == null){
      //create texture
      this.tex = gl.createTexture();
      gl.bindTexture(gl.TEXTURE_2D, this.tex);

      // Set the parameters so we don't need mips
      gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
      gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);
      gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
      gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);

      // Upload the image into the texture.
      var mipLevel = 0;               // the largest mip
      var internalFormat = gl.RGBA;   // format we want in the texture
      var srcFormat = gl.RGBA;        // format of data we are supplying
      var srcType = gl.UNSIGNED_BYTE  // type of data we are supplying
      gl.texImage2D(gl.TEXTURE_2D,
                    mipLevel,
                    internalFormat,
                    srcFormat,
                    srcType,
                    this.image)
    }
    return this.tex
  }
}
