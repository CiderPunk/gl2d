export interface IResMan {
  loadResource(type:string, src:string):IResource
  notifyComplete(res:IResource):void
  isComplete():boolean
  registerNotify(cb:()=> void):void
}

export interface IResource{
  url:string
}
export interface IView{
  gl:WebGL2RenderingContext
}

export interface ISpriteRenderer{
  draw(tex:ITexture,x:number, y:number, w:number, h:number):void
  flush():void
}

export interface ITexture extends IResource {
  getTexture(gl:WebGL2RenderingContext):WebGLTexture
  getTexCoords():Array<number>
}

export interface IMap extends IResource {
  tileWidth:number
  tileHeight:number
  resMan:IResMan
  getTile(index:number):ITexture
  setTile(index:number, tx:ITexture):void
  getLayer(name:string):IMapLayer
  draw(worldX:number, worldY:number, screenX:number, screenY:number, width:number, height:number, batch:ISpriteRenderer):void
}

export interface IMapLayer{
  draw(offsX:number, offsY:number, tileX:number, tileY:number,tilesWide:number, tilesHigh:number, batch:ISpriteRenderer):void
}