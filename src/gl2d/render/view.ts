export class View{

  public readonly gl:WebGL2RenderingContext
  

  public width:number
  public height:number
  public constructor(private canvas:HTMLCanvasElement ){  
    canvas.width = parseInt(canvas.style.width);
    canvas.height = parseInt(canvas.style.height);
    this.gl = canvas.getContext("webgl2")
    if (!this.gl){
      throw new Error("No WebGL2")
    } 
    this.resize()
  }

  public clear(r:number = 1, g:number = 1, b:number = 1, a:number = 1){
    // Clear the canvas
    this.gl.clearColor(r, g, b, a);
    this.gl.clear(WebGL2RenderingContext.COLOR_BUFFER_BIT);

  }

  public resize() {
    // Lookup the size the browser is displaying the canvas.
    var displayWidth  = this.canvas.clientWidth;
    var displayHeight = this.canvas.clientHeight;
  
    // Check if the canvas is not the same size.
    if (this.canvas.width  !== displayWidth ||
        this.canvas.height !== displayHeight) {
      // Make the canvas the same size
      this.canvas.width  = displayWidth;
      this.canvas.height = displayHeight;
    }
    this.gl.viewport(0, 0, this.gl.canvas.width, this.gl.canvas.height)

    this.width = this.gl.canvas.width
    this.height = this.gl.canvas.height
  }


}

