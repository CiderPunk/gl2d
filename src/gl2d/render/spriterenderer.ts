import * as WGLUtils from "../utils/webglutils"
import { M3 } from "../math/m3";
import { Texture } from "../gfx/texture"
import { IView, ITexture, ISpriteRenderer } from "../interfaces"
import { Float32Buffer } from "../containers/float32buffer"

export class SpriteRenderer implements ISpriteRenderer{

  private prog:WebGLProgram
  public readonly projection:M3
  private readonly gl:WebGL2RenderingContext

  private positionAttributeLocation:number
  private texCoordAttributeLocation:number
  private imageLocation:WebGLUniformLocation
  private projectionMatrixLocation:WebGLUniformLocation
  private resolutionUniformLocation:WebGLUniformLocation
  private colorLocation: WebGLUniformLocation

  //private vertexBuffer:Float32Array = new Float32Array(2000)
  //private texCoordBuffer:Float32Array = new Float32Array(2000)
  
  private positionBuffer:WebGLBuffer
  private texCoordBuffer:WebGLBuffer

  private posBuffer:Float32Buffer
  private texBuffer:Float32Buffer

  private currentTexture:WebGLTexture
  private drawCount:number = 0
  private static readonly MaxDrawCount = 200000
//private  positions:Float32Array
//private positionPointer:number = 0

  public constructor(view:IView, private bufferSize:number = SpriteRenderer.MaxDrawCount){  
    this.gl = view.gl
    let gl = this.gl
    let vertShader = WGLUtils.compileShader(gl, this.vertShaderSource, WebGL2RenderingContext.VERTEX_SHADER)
    let fragShader = WGLUtils.compileShader(gl, this.fragShaderSource, WebGL2RenderingContext.FRAGMENT_SHADER)
    this.prog = WGLUtils.createProgram(gl, vertShader, fragShader)

    this.positionAttributeLocation = gl.getAttribLocation(this.prog, "a_position")
    this.texCoordAttributeLocation = gl.getAttribLocation(this.prog, "a_texcoord")
    this.colorLocation = this.gl.getUniformLocation(this.prog, "u_colour")
    this.projectionMatrixLocation = gl.getUniformLocation(this.prog, "u_projmatrix")
    this.imageLocation = gl.getUniformLocation(this.prog, "u_image")

    this.projection = new M3().setProjection(gl.canvas.width, gl.canvas.height)

    gl.useProgram(this.prog)
    gl.uniformMatrix3fv(this.projectionMatrixLocation, false, this.projection.values)
    
    this.positionBuffer = this.gl.createBuffer()
    this.texCoordBuffer = this.gl.createBuffer()

    this.posBuffer = new Float32Buffer(this.bufferSize * 12) 
    this.texBuffer = new Float32Buffer(this.bufferSize * 12)

    //let vao = this.gl.createVertexArray()
    //gl.bindVertexArray(vao)
  }


  public flush(){
    if (this.drawCount == 0) return
    let gl = this.gl
    gl.bindBuffer(this.gl.ARRAY_BUFFER, this.positionBuffer)
    gl.bufferData(this.gl.ARRAY_BUFFER, this.posBuffer.values, this.gl.DYNAMIC_DRAW)
    gl.enableVertexAttribArray(this.positionAttributeLocation)
    gl.vertexAttribPointer(this.positionAttributeLocation, 2, this.gl.FLOAT, false, 0, 0)

    gl.bindBuffer(this.gl.ARRAY_BUFFER, this.texCoordBuffer)
    gl.bufferData(this.gl.ARRAY_BUFFER, this.texBuffer.values, this.gl.DYNAMIC_DRAW)
    gl.enableVertexAttribArray(this.texCoordAttributeLocation)
    gl.vertexAttribPointer(this.texCoordAttributeLocation, 2, this.gl.FLOAT, false, 0, 0)

    //gl.viewport(0, 0, this.gl.canvas.width, this.gl.canvas.height)
    gl.useProgram(this.prog)
    //gl.uniform2f(this.resolutionUniformLocation, 640,480);
    // Set a random color.
    //gl.uniform4f(this.colorLocation, Math.random(), Math.random(), Math.random(), 1)

    //let texture = tex.getTexture(gl)
    gl.activeTexture(gl.TEXTURE0)
    gl.bindTexture(gl.TEXTURE_2D,this.currentTexture)
    gl.uniform1i(this.imageLocation, 0) 

    var primitiveType = WebGL2RenderingContext.TRIANGLES;
    var offset = 0;
    var count = 6 * this.drawCount;
    gl.drawArrays(primitiveType, offset, count);
    this.posBuffer.Clear()
    this.texBuffer.Clear()
    this.drawCount = 0
  }

  public draw(tex:ITexture,x:number, y:number, w:number, h:number){
    let gl = this.gl
    let texture = tex.getTexture(gl)
    if (texture != this.currentTexture){
      this.flush()
      this.currentTexture = texture
    }  

    this.posBuffer.LoadVerts([
      x, y,
      x+w, y,
      x, y+h,
      x, y+h,
      x+w, y,
      x+w, y+h
    ])
    this.texBuffer.LoadVerts(tex.getTexCoords())
    this.drawCount++
    if (this.drawCount == this.bufferSize){
      this.flush()
    }
  }


private vertShaderSource =`#version 300 es
// an attribute will receive data from a buffer
in vec2 a_position;
in vec2 a_texcoord;
out vec2 v_texcoord;
//view transform
uniform mat3 u_projmatrix;
void main() {
  gl_Position = vec4((u_projmatrix * vec3(a_position, 1)).xy, 0, 1);
  v_texcoord = a_texcoord;
}
`

private fragShaderSource = `#version 300 es
precision mediump float;
in vec2 v_texcoord;
uniform sampler2D u_image;
out vec4 outColor;
void main() {
   outColor = texture(u_image, v_texcoord);
}`


}