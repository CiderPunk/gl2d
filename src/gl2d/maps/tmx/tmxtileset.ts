import { PathUtils } from "../../utils/pathutils"
import { ITexture, IMap } from "../../interfaces"
import { ResNames } from "../../resnames"
import { TxPart } from "../../gfx/txpart"

export class TmxTileSet {

  public readonly name:string
  public readonly tileWidth:number
  public readonly tileHeight:number
  public readonly first:number
  public readonly count:number
  //public readonly cols:number
  private readonly imgwidth:number
  private readonly imgheight:number
  private readonly texture:ITexture
  public readonly tiles = new Map<number,TxPart>()

  public constructor(private map:IMap, node:Element){
    this.name = node.getAttribute("name")
    this.tileWidth = parseInt(node.getAttribute("tilewidth"))
    this.tileHeight = parseInt(node.getAttribute("tileheight"))
    this.first = parseInt(node.getAttribute("firstgid"))
    this.count = parseInt(node.getAttribute("tilecount"))
    //this.cols = parseInt(node.getAttribute("columns"))

    let rm = this.map.resMan 

    //rm.registerNotify(()=>this.buildTiles)
    let imgnodes = node.getElementsByTagName("image")
    if (imgnodes.length > 1){
      throw new Error("Too many images in tileset")
    }
    let img = imgnodes[0]
    let src = img.getAttribute("source")
    this.imgwidth = parseInt(img.getAttribute("width"))
    this.imgheight = parseInt(img.getAttribute("height")) 
    this.texture = rm.loadResource(ResNames.TEXTURE, PathUtils.getReferedPath(this.map.url, src)) as ITexture
    this.buildTiles()
  }

  public buildTiles(){
    //let tw = this.tileWidth / this.imgwidth
    //let th = this.tileHeight / this.imgheight
    let cols = this.imgwidth / this.tileWidth
    let rows = this.imgheight / this.tileHeight
    let tw = 1 / cols 
    let th = 1 / rows
    for (let i = 0;  i < this.count; i++){
      let col = i % cols
      let row = Math.floor(i / cols)
      this.map.setTile(i + this.first, new TxPart(this.texture,col*tw,row*th,tw,th))
    }
  }
  
}