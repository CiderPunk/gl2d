import { IResource, IResMan, IMap, IMapLayer, ITexture, ISpriteRenderer } from "../../interfaces"
import { XmlResource } from "../../resources/xmlresource"
import { TmxTileSet } from "./tmxtileset"
import { TmxLayer } from "./tmxlayer"

export class TmxMap extends XmlResource implements IMap{
  resMan: IResMan;
  public readonly tiles = new Map<number,ITexture>()
  url: string;
  private width: number
  private height:number
  public tileWidth:number
  public tileHeight:number
  private tileSets = new Array<TmxTileSet>()
  private layers = new Array<TmxLayer>()

  parseXml(doc: XMLDocument): void {
    let root = doc.documentElement
    this.width = parseInt(root.getAttribute("width"))
    this.height = parseInt(root.getAttribute("height"))
    this.tileWidth = parseInt(root.getAttribute("tilewidth"))
    this.tileHeight = parseInt(root.getAttribute("tileheight"))
    let tilesetNodes = root.getElementsByTagName("tileset")
    for (let ts of tilesetNodes){
      let nts = new TmxTileSet(this, ts)
      this.tileSets.push(nts)  
    }
    let layerNodes = root.getElementsByTagName("layer")
    for (let layerNode of layerNodes){
      let layer = new TmxLayer(this, layerNode)
      this.layers.push(layer)
    }
  }

  public draw(worldX:number, worldY:number, screenX:number, screenY:number, width:number, height:number, batch:ISpriteRenderer):void{
    let tx = Math.floor(worldX / this.tileWidth)
    let ty = Math.floor(worldY / this.tileHeight)
    let tw = Math.ceil(width / this.tileWidth) + 1
    let th = Math.ceil(height / this.tileHeight) + 1
    let offsX = screenX - (worldX % this.tileWidth) 
    let offsY = screenY - (worldY % this.tileHeight)
    this.layers.forEach(layer => {
      layer.draw(offsX,offsY, tx,ty, tw,th, batch)
    });
  }

  public static loader(url:string, resman:IResMan):TmxMap{
    return new TmxMap(url, resman)
  }

  setTile(index:number, tx:ITexture){
    this.tiles.set(index,tx)
  }

  getTile(index: number): ITexture {
    return this.tiles.get(index)
  }

  getLayer(name: string): IMapLayer {
    return this.layers.find((l)=>l.name == name)
  }

  getTileSet(name:string):TmxTileSet{
    return this.tileSets.find((ts)=>ts.name == name)
  }
  
}