import { ITexture, IMap, IMapLayer, ISpriteRenderer } from "../../interfaces"
import * as pako from "pako"

export class TmxLayer implements IMapLayer{


  public readonly name: string
  public readonly width:number
  public readonly height:number
  public visible:boolean
  private mapTiles:Uint32Array

  public constructor(private map:IMap, node:Element){
    this.name = node.getAttribute("name")
    this.width = parseInt(node.getAttribute("width"))
    this.height = parseInt(node.getAttribute("height"))
    this.visible = node.getAttribute("visible") != "0"
    let datanodes = node.getElementsByTagName("data")
    if (datanodes.length > 0){
      let data  = datanodes[0].textContent.trim()
      let decoded = window.atob(data)
      let inflated = pako.inflate(decoded)
      this.mapTiles = new Uint32Array(inflated.buffer)
    }
  }

  draw(offsX:number, offsY:number, 
    tileX:number, tileY:number,
    tilesWide:number, tilesHigh:number,
    batch:ISpriteRenderer):void{

    if (!this.visible) return
    /*
      let minX = tileX < 0 ? 0 : tileX
      let minY = tileY < 0 ? 0 : tileY
      let maxX = this.width > tilesWide ? tilesWide : this.width
      let maxY = this.height > tilesHigh ? tilesHigh : this.height
    */
  

    for (let y = 0; y < tilesHigh; y++){
      for(let x = 0; x < tilesWide; x++){
        let tx = x + tileX
        let ty = y + tileY
        //bounds test
        if (tx >= 0 && tx < this.width && ty >= 0 && ty < this.height){
          let tileindex = this.mapTiles[(ty * this.width) + tx]
          if (tileindex > 0){
            let tile = this.map.getTile(tileindex)
            batch.draw(tile, 
              offsX + (x * this.map.tileWidth), 
              offsY + (y * this.map.tileHeight), 
              this.map.tileWidth, this.map.tileHeight)
          }
        }
        
      }
    }
  }
}