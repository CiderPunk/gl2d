import { ResMan } from "./resources/resman"
import { Texture } from "./gfx/texture"
import { TmxMap } from "./maps/tmx/tmxmap"
import { ResNames } from "./resnames"

export namespace Gl2d{

  export function Init(){
    ResMan.instance.registerResourceType(ResNames.TEXTURE, Texture.loader)
    ResMan.instance.registerResourceType(ResNames.TMXMAP, TmxMap.loader)
  }

}