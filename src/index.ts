import { Gl2d } from "./gl2d/index"
import { ResNames } from "./gl2d/resnames"
import { ResMan } from "./gl2d/resources/resman"
import { SpriteRenderer } from "./gl2d/render/spriterenderer"
import { View } from "./gl2d/render/view"
import { ITexture, ISpriteRenderer, IMap } from "./gl2d/interfaces"


Gl2d.Init();
let canvas = document.getElementById("gl2d") as HTMLCanvasElement
let view = new View(canvas)
let rend = new SpriteRenderer(view,200)
view.clear()
let weasel = ResMan.instance.loadResource(ResNames.TEXTURE, "/img/weasel.png") as ITexture
let turnip = ResMan.instance.loadResource(ResNames.TEXTURE, "/img/turnip.jpeg") as ITexture

let map = ResMan.instance.loadResource(ResNames.TMXMAP, "/maps/level1.tmx") as IMap

ResMan.instance.registerNotify(()=>{ 
 
drawScene(0)
  /*
  let toggle = false
  for(let x =0; x <102; x++){
    for(let y =0; y < 76; y++){
      rend.draw(weasel, x * 10 ,y * 10,10,10) 
      rend.draw(turnip, x * 10 ,y * 10,10,10) 
      //rend.draw(toggle ? weasel: turnip, x * 20 ,y * 20,20,20) 
    }
    toggle = !toggle
  }
  rend.flush()
  */
})



let x = 0
let lastTime = 0
let dt = 0
let dir = 1
function drawScene(time:number){
  dt = time - lastTime 
  lastTime = time


  x += 0.3 * dt * dir

  if (x > 640) dir = -1
   
   if (x < -200) dir = 1 
  view.clear()

/*
  for(let x =0; x <102; x++){
    for(let y =0; y < 76; y++){
      rend.draw(weasel, x * 10 ,y * 10,10,10) 
      rend.draw(turnip, x * 10 ,y * 10,10,10) 
      //rend.draw(toggle ? weasel: turnip, x * 20 ,y * 20,20,20) 
    }
  
  }
*/
  map.draw(x,0,0,0,view.width, view.height, rend)
  rend.flush()
  window.requestAnimationFrame(drawScene)
}
