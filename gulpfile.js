var gulp = require('gulp');
var browserSync = require('browser-sync');
var ts = require('gulp-typescript');
var sourcemaps = require('gulp-sourcemaps');
var reload      = browserSync.reload;
var clientFiles = "./src/**/*.ts";

var tsProject = ts.createProject({
  module:"amd",
  noImplicitAny: true,
  outFile:"gl2d.js",
  target:"es6"
});

gulp.task('browser-sync', function(){
  browserSync.init({ 
    server:{
      baseDir: "./public"
    },
    serveStatic:[{ 
      route:'vendor',
      dir: './node_modules'
    }]
  })
});

gulp.task('buildClient', function(){  
  return gulp.src([clientFiles])
    .pipe(sourcemaps.init())
    .pipe(tsProject())
    .pipe(sourcemaps.write({ sourceRoot:"." }))
    .pipe(gulp.dest('public/script'))
    .pipe(reload({stream:true}));
});


gulp.task('watch', ['buildClient'], function(){
  gulp.watch(clientFiles,['buildClient']);
});


gulp.task('default', ['buildClient', 'watch', 'browser-sync']);

